DROP DATABASE IF EXISTS `shopcar`;
CREATE DATABASE `shopcar`;
USE `shopcar`;

DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE `veiculo` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`tpVeiculo` VARCHAR(30) NOT NULL DEFAULT '0',
	`anoFabricacao` INT(11) NOT NULL DEFAULT '0',
	`placa` VARCHAR(8) NOT NULL DEFAULT '0',
	`numeroChassi` VARCHAR(30) NOT NULL DEFAULT '0',
	`marca` VARCHAR(30) NOT NULL DEFAULT '0',
	`modelo` VARCHAR(30) NOT NULL DEFAULT '0',
	`cor` VARCHAR(30) NOT NULL DEFAULT '0',
	`quiloMetros` INT(11) NOT NULL DEFAULT '0',
	`qtdePortas` INT(11) NOT NULL DEFAULT '0',
	`numeroMarcha` INT(11) NOT NULL DEFAULT '0',
	`valor` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`vendido` TINYINT(4) NOT NULL DEFAULT '0',
	`numeroEixos` INT(11) NULL DEFAULT NULL,
	`cilindradas` INT(11) NULL DEFAULT NULL,
	`numeroAssentos` INT(11) NULL DEFAULT NULL,
	`capacidadeMaxCarga` INT(11) NULL DEFAULT NULL,
	`tipoCarroceria` VARCHAR(30) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `placa` (`placa`),
	INDEX `tpVeiculo` (`tpVeiculo`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
