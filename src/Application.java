import dao.DAO;
import dao.DAOVeiculos;
import entities.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import view.FormInicial;

/**
 * @author Carlos
 *
 */
public class Application
{
	private static ArrayList<Veiculo> veiculos;
	
    /**
     * @param args
     */
    public static void main(String[] args)
    {
    	try
    	{
	    	veiculos = new ArrayList<Veiculo>();
	    	
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			Connection sql = DriverManager.getConnection("jdbc:mysql://localhost:3306/shopcar", "root", "");
	
			Statement stmt = sql.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM veiculo");
			
			while(rs.next())
			{
				Veiculo v = null;
				switch(rs.getString(2))
				{
					case "Carro":
						v = new Carro();
						break;
					case "Caminhao":
						Caminhao cho = new Caminhao();
						cho.setNumeroEixos(rs.getInt(14));
						cho.setTipoCarroceria(rs.getString(18));
						cho.setCapacidadeMaxCarga(rs.getInt(17));
						v = cho;
						break;
					case "Caminhonete":
						Caminhonete cnt = new Caminhonete();
						cnt.setNumeroEixos(rs.getInt(14));
						cnt.setCapacidadeMaxCarga(rs.getInt(17));
						v = cnt;
						break;
					case "Moto":
						Moto m = new Moto();
						m.setCilindradas(rs.getInt(15));
						v = m;
						break;
					case "Onibus":
						Onibus o = new Onibus();
						o.setNumeroAssentos(rs.getInt(16));
						o.setNumeroEixos(rs.getInt(14));
						v = o;
						break;
					default:
						v = new Carro();
						break;
				}
				v.setId(rs.getInt(1));
				v.setAnoFabricacao(rs.getInt(3));
				v.setPlaca(rs.getString(4));
				v.setNumeroChassi(rs.getString(5));
				v.setMarca(Marca.valueOf(rs.getString(6)));
				v.setModelo(rs.getString(7));
				v.setCor(Cor.valueOf(rs.getString(8)));
				v.setQuiloMetros(rs.getInt(9));
				v.setQtdePortas(rs.getInt(10));
				v.setNumeroMarcha(rs.getInt(11));
				v.setValor(rs.getFloat(12));
				v.setVendido(rs.getBoolean(13));
	
				veiculos.add(v);
			}
	
			DAOVeiculos.list = veiculos;
    	}
    	catch(Exception ex)
    	{
    		System.err.println(ex);
    	}
    	
    	// Configurando a apar�ncia da aplica��o para corresponder
		// � apar�ncia do sistema operacional 
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			FormInicial formInicial = new FormInicial();
			formInicial.exibirFrame();
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
}
	