package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import entities.*;


public class DAO<T extends Veiculo> {

	private Connection sqlConnection;
	
	public DAO() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		this.sqlConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/shopcar", "root", "");
	}
	
	public static List<Veiculo> veiculosDeTeste;

	/**
	 * Salva um veiculo no banco de dados.
	 * @param veiculo
	 * @throws SQLException
	 */
	public void salvar(T veiculo) throws SQLException {
		PreparedStatement stmt = this.sqlConnection.prepareStatement("INSERT INTO veiculo"
				+ "(tpVeiculo, anoFabricacao, placa, numeroChassi, marca,"
				+ "modelo, cor, quiloMetros, qtdePortas, numeroMarcha, valor, vendido) values ("
				+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		stmt.setString(1, veiculo.getClass().getSimpleName());
		stmt.setInt(2, veiculo.getAnoFabricacao());
		stmt.setString(3, veiculo.getPlaca());
		stmt.setString(4, veiculo.getNumeroChassi());
		stmt.setString(5, veiculo.getMarca().toString());
		stmt.setString(6, veiculo.getModelo());
		stmt.setString(7, veiculo.getCor().toString());
		stmt.setInt(8, veiculo.getQuiloMetros());
		stmt.setInt(9, veiculo.getQtdePortas());
		stmt.setInt(10, veiculo.getNumeroMarcha());
		stmt.setFloat(11, veiculo.getValor());
		stmt.setBoolean(12, veiculo.getVendido());
		stmt.execute();
		
		Statement stm = this.sqlConnection.createStatement();
		ResultSet rs = stm.executeQuery("SELECT LAST_INSERT_ID();");
		
		rs.next();
		int lastId = rs.getInt(1);

		PreparedStatement pstmt;
		
		if(veiculo instanceof Caminhao)
		{
			Caminhao c = (Caminhao)veiculo;
			pstmt = this.sqlConnection.prepareStatement("UPDATE veiculo SET numeroEixos = ?,"
					+ "tipoCarroceria = ?, capacidadeMaxCarga = ? WHERE id = ?");
			pstmt.setInt(1, c.getNumeroEixos());
			pstmt.setString(2, c.getTipoCarroceria());
			pstmt.setInt(3, c.getCapacidadeMaxCarga());
			pstmt.setInt(4, c.getId());
			pstmt.execute();
		}
		else if(veiculo instanceof Caminhonete)
		{
			Caminhonete c = (Caminhonete)veiculo;
			pstmt = this.sqlConnection.prepareStatement("UPDATE veiculo SET numeroEixos = ?,"
					+ "capacidadeMaxCarga = ? WHERE id = ?");
			pstmt.setInt(1, c.getNumeroEixos());
			pstmt.setInt(2, c.getCapacidadeMaxCarga());
			pstmt.setInt(3, c.getId());
			pstmt.execute();
		}
		else if(veiculo instanceof Moto)
		{
			Moto c = (Moto)veiculo;
			pstmt = this.sqlConnection.prepareStatement("UPDATE veiculo SET cilindradas = ?,"
					+ "WHERE id = ?");
			pstmt.setInt(1, c.getCilindradas());
			pstmt.setInt(2, c.getId());
			pstmt.execute();
		}
		else if(veiculo instanceof Onibus)
		{
			Onibus c = (Onibus)veiculo;
			pstmt = this.sqlConnection.prepareStatement("UPDATE veiculo SET numeroAssentos = ?, numeroEixos = ? "
					+ "WHERE id = ?");
			pstmt.setInt(1, c.getNumeroAssentos());
			pstmt.setInt(2, c.getNumeroEixos());
			pstmt.setInt(3, c.getId());
			pstmt.execute();
		}
		
		veiculosDeTeste.add(veiculo);
	}

	public Veiculo getVeiculo(int id) {
		return getVeiculosDeTeste().get(id);
	}
	
	public Veiculo getVeiculo(String placa) {
		return getVeiculosDeTeste().get(0);
	}

	public List<Veiculo> getVeiculosNaoVendidos() {
		return getVeiculosDeTeste();
	}

	public List<Veiculo> getVeiculosPorAno(int ano) {
		return getVeiculosDeTeste();
	}

	public List<Veiculo> getVeiculosPorMarca(Marca marca) {
		return getVeiculosDeTeste();
	}

	public List<Veiculo> getVeiculosPorQuilometragem(int quilometragem) {
		return getVeiculosDeTeste();
	}

	public List<Veiculo> getVeiculosPorModelo(String modelo) {
		return getVeiculosDeTeste();
	}

	public static List<Veiculo> getVeiculosDeTeste() {

		if (veiculosDeTeste == null) {
			veiculosDeTeste = new ArrayList<Veiculo>();

			/*Veiculo v1 = new Veiculo();
			v1.setAnoFabricacao(1998);
			v1.setCor(Cor.AMARELO);
			v1.setMarca(Marca.CHEVROLET);
			v1.setModelo("Modelo 01");
			v1.setNumeroChassi("V6IVH67VI5YF");
			v1.setNumeroMarcha(5);
			v1.setPlaca("8DF78DF");
			v1.setPotenciaCavalos(500);
			v1.setQtdePortas(4);
			v1.setQuiloMetros(0);
			v1.setValor(10000.0f);
			v1.setVendido(true);
			veiculosDeTeste.add(v1);

			Veiculo v2 = new Veiculo();
			v2.setAnoFabricacao(1999);
			v2.setCor(Cor.AZUL);
			v2.setMarca(Marca.VOLKSWAGEM);
			v2.setModelo("Modelo 02");
			v2.setNumeroChassi("9KI8DHVI5YF");
			v2.setNumeroMarcha(6);
			v2.setPlaca("B4B2C4I2");
			v2.setPotenciaCavalos(600);
			v2.setQtdePortas(2);
			v2.setQuiloMetros(100);
			v2.setValor(25000.0f);
			v2.setVendido(false);
			veiculosDeTeste.add(v2);

			Veiculo v3 = new Veiculo();
			v3.setAnoFabricacao(2010);
			v3.setCor(Cor.VERDE);
			v3.setMarca(Marca.MERCEDES);
			v3.setModelo("Modelo 03");
			v3.setNumeroChassi("C39BM1L9CF56DO7");
			v3.setNumeroMarcha(4);
			v3.setPlaca("V4M28BL9");
			v3.setPotenciaCavalos(900);
			v3.setQtdePortas(5);
			v3.setQuiloMetros(0);
			v3.setValor(45000.0f);
			v3.setVendido(false);
			veiculosDeTeste.add(v3);

			Veiculo v4 = new Veiculo();
			v4.setAnoFabricacao(2005);
			v4.setCor(Cor.BRANCO);
			v4.setMarca(Marca.MERCEDES);
			v4.setModelo("Modelo 04");
			v4.setNumeroChassi("NV5J3F5J3V3");
			v4.setNumeroMarcha(6);
			v4.setPlaca("VB3N3K");
			v4.setPotenciaCavalos(600);
			v4.setQtdePortas(3);
			v4.setQuiloMetros(50);
			v4.setValor(30000.0f);
			v4.setVendido(true);
			veiculosDeTeste.add(v4);

			Veiculo v5 = new Veiculo();
			v5.setAnoFabricacao(2013);
			v5.setCor(Cor.PRETO);
			v5.setMarca(Marca.CHEVROLET);
			v5.setModelo("Modelo 05");
			v5.setNumeroChassi("L584F5J3V3");
			v5.setNumeroMarcha(6);
			v5.setPlaca("NJ2N3K");
			v5.setPotenciaCavalos(500);
			v5.setQtdePortas(4);
			v5.setQuiloMetros(0);
			v5.setValor(55000.0f);
			v5.setVendido(true);
			veiculosDeTeste.add(v5);*/
		}

		return veiculosDeTeste;
	}
}
