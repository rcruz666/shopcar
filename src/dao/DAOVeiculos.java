package dao;

import java.util.ArrayList;

import entities.Marca;
import entities.Veiculo;

	public class DAOVeiculos
	{
		public static ArrayList<Veiculo> list;
		
		public static Veiculo getPlaca(String placa)
		{
			for(Veiculo i : list)
			{
				if(i.getPlaca().compareTo(placa) == 0)
				{
					return i;
				}
			}
			return null;
		}
		
		public static ArrayList<Veiculo> getPorModelo(String modelo)
		{
			ArrayList<Veiculo> v = new ArrayList<Veiculo>();
			for(Veiculo i : list)
			{
				if(i.getModelo().compareTo(modelo) == 0)
				{
					v.add(i);
				}
			}
			return v;
		}
		
		public static ArrayList<Veiculo> getPorMarca(Marca m)
		{
			ArrayList<Veiculo> v = new ArrayList<Veiculo>();
			for(Veiculo i : list)
			{
				if(i.getMarca().compareTo(m) == 0)
				{
					v.add(i);
				}
			}
			return v;
		}
		
		public static ArrayList<Veiculo> getPorKM(int KM)
		{
			ArrayList<Veiculo> v = new ArrayList<Veiculo>();
			for(Veiculo i : list)
			{
				if(i.getQuiloMetros() <= KM)
				{
					v.add(i);
				}
			}
			return v;
		}
		
		public static ArrayList<Veiculo> getPorAno(int ano)
		{
			ArrayList<Veiculo> v = new ArrayList<Veiculo>();
			for(Veiculo i : list)
			{
				if(i.getAnoFabricacao() == ano)
				{
					v.add(i);
				}
			}
			return v;
		}
		
		public static ArrayList<Veiculo> getNaoVendidos()
		{
			ArrayList<Veiculo> v = new ArrayList<Veiculo>();
			for(Veiculo i : list)
			{
				if(i.getVendido() == false)
				{
					v.add(i);
					System.out.println(i.toString());
				}
			}
			return v;
		}
	}
