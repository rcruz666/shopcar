package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Caminhao extends Veiculo
{
    /**
     * Numeros de eixos do caminh�o.
     */
    private int numeroEixos;
    
    /**
     * Tipo de carroceria.
     */
    private String tipoCarroceria;
    
    
    private int capacidadeMaxCarga;

    /**
	 * Obt�m o(a) capacidadeMaxCarga do(a) Caminhao
	 * @return o(a) capacidadeMaxCarga do(a) Caminhao
	 */
	public int getCapacidadeMaxCarga() {
		return capacidadeMaxCarga;
	}

	/**
	 * Define o(a) capacidadeMaxCarga do(a) Caminhao
	 * @param capacidadeMaxCarga o(a) capacidadeMaxCarga a ser definido
	 */
	public void setCapacidadeMaxCarga(int capacidadeMaxCarga) {
		this.capacidadeMaxCarga = capacidadeMaxCarga;
	}

	/**
     * Define o n�mero de eixos para o Caminh�o.
     * @param numeroEixos
     */
    public void setNumeroEixos(int numeroEixos)
    {
        this.numeroEixos = numeroEixos;
    }

    /**
     * Obt�m o numero de eixos do caminh�o.
     * @return
     */
    public int getNumeroEixos()
    {
        return this.numeroEixos;
    }
    
    /**
     * Define o tipo de carroceria.
     * @param tipoCarroceria
     */
    public void setTipoCarroceria(String tipoCarroceria)
    {
        this.tipoCarroceria = tipoCarroceria;
    }

    /**
     * Obt�m o tipo de carroceria.
     * @return
     */
    public String getTipoCarroceria()
    {
        return this.tipoCarroceria;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Caminhao [numeroEixos=" + numeroEixos + ", tipoCarroceria="
				+ tipoCarroceria + ", capacidadeMaxCarga=" + capacidadeMaxCarga
				+ ", " + super.toString();
	}
    
    
}
