package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Caminhonete extends Veiculo
{
    
    private int numeroEixos;
    private int capacidadeMaxCarga;

    /**
	 * Obt�m o(a) capacidadeMaxCarga do(a) Caminhao
	 * @return o(a) capacidadeMaxCarga do(a) Caminhao
	 */
	public int getCapacidadeMaxCarga() {
		return capacidadeMaxCarga;
	}

	/**
	 * Define o(a) capacidadeMaxCarga do(a) Caminhao
	 * @param capacidadeMaxCarga o(a) capacidadeMaxCarga a ser definido
	 */
	public void setCapacidadeMaxCarga(int capacidadeMaxCarga) {
		this.capacidadeMaxCarga = capacidadeMaxCarga;
	}
    
	/**
	 * Obt�m o(a) numeroEixos do(a) Carro
	 * @return o(a) numeroEixos do(a) Carro
	 */
	public int getNumeroEixos() {
		return numeroEixos;
	}
	/**
	 * Define o(a) numeroEixos do(a) Carro
	 * @param numeroEixos o(a) numeroEixos a ser definido
	 */
	public void setNumeroEixos(int numeroEixos) {
		this.numeroEixos = numeroEixos;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Caminhonete [numeroEixos=" + numeroEixos
				+ ", capacidadeMaxCarga=" + capacidadeMaxCarga
				+ ", " + super.toString();
	}
	
	
}
