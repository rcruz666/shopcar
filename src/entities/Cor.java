package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public enum Cor
{
    PRETO("Preto"),
    BRANCO("Branco"),
    AMARELO("Amarelo"),
    AZUL("Azul"),
    VERMELHO("Vermelho"),
    VERDE("Verde");
    
    @SuppressWarnings("unused")
    private String desc;
    
    Cor(String desc)
    {
        this.desc = desc;
    }
}
