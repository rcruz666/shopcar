package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public interface ItemVendido
{
    /**
     * Define o status do item. Se foi vendido ou n�o.
     * @param bVendido Verdadeiro para vendido. Caso contrario, falso.
     */
    void setVendido(boolean vendido);
    
    /**
     * Obt�m a informa��o se o item foi vendido.
     * @return Retorna falso caso n�o tenha sido vendido.
     */
    boolean getVendido();
}
