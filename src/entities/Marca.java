package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public enum Marca
{
    CHEVROLET("Chevrolet"),
    VOLKSWAGEM("Volkswagem"),
    FIAT("Fiat"),
    MERCEDES("Mercedes");
    
    @SuppressWarnings("unused")
    private String desc;
    
    Marca(String desc)
    {
        this.desc = desc;
    }
}
