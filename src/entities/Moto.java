package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Moto extends Veiculo
{
    public Moto()
    {
        this.setQtdePortas(0);
    }
    
    /**
     * Cilindradas da moto.
     */
    private int cilindradas;
    
    /**
     * Define as cilindradas para moto.
     * @param cilindradas
     */
    public void setCilindradas(int cilindradas)
    {
        this.cilindradas = cilindradas;
    }
    
    /**
     * Obt�m as cilindradas da moto.
     * @return
     */
    public int getCilindradas()
    {
        return this.cilindradas;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Moto [cilindradas=" + cilindradas + ", "
				+ super.toString();
	}
    
    
}
