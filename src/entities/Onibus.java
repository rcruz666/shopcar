package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Onibus extends Veiculo
{
	/**
	 * Quantidade de assentos do �nibus
	 */
	private int numeroAssentos;
	
	/**
	 * Quantidade de eixos do �nibus
	 */
	private int numeroEixos;

	/**
	 * Obt�m o numero de assentos do Onibus
	 * @return o numero de assentos do Onibus
	 */
	public int getNumeroAssentos() {
		return numeroAssentos;
	}

	/**
	 * Define o numero de assentos do Onibus
	 * @param numeroAssentos o numero de assentos a ser definido
	 */
	public void setNumeroAssentos(int numeroAssentos) {
		this.numeroAssentos = numeroAssentos;
	}

	/**
	 * Obt�m o n�mero de eixos do Onibus
	 * @return o n�mero de eixos do Onibus
	 */
	public int getNumeroEixos() {
		return numeroEixos;
	}

	/**
	 * Define o n�mero de eixos do Onibus
	 * @param n�mero de eixos o n�mero de eixos a ser definido
	 */
	public void setNumeroEixos(int numeroEixos) {
		this.numeroEixos = numeroEixos;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Onibus [numeroAssentos=" + numeroAssentos + ", numeroEixos="
				+ numeroEixos + ", " + super.toString();
	}

}
