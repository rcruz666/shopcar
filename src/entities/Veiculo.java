package entities;
/**
 * 
 */

/**
 * @author Carlos
 *
 */
public class Veiculo implements ItemVendido
{
	/**
	 * Id do ve�culo
	 */
	private int id;

	/**
     * Ano de fabrica��o do veiculo.
     */
    private int anoFabricacao;
    
    /**
     * Placa do veiculo.
     */
    private String placa;
    
    /**
     * Numero do chassi do veiculo.
     */
    private String numeroChassi;
    
    /**
     * Marca do veiculo
     */
    private Marca marca;
    
    /**
     * Modelo do veiculo.
     */
    private String modelo;
    
    /**
     * Cor para o veiculo.
     */
    private Cor cor;
    
    /**
     * Quilometragem para o veiculo.
     */
    private int quiloMetros;
    
    /**
     * N�mero de portas.
     */
    private int qtdePortas;
    
    /**
     * Quantidade de marchas que o veiculo possui.
     */
    private int numeroMarcha;
    
    /**
     * Valor para o veiculo.
     */
    private float valor;
    
    /**
     * Define se o veiculo foi vendido.
     */
    private boolean vendido;
    
    /**
     * Pontencia do carro em cavalos.
     */
    private int potenciaCavalos;
    
    /**
	 * Obt�m o(a) id do(a) Veiculo
	 * @return o(a) id do(a) Veiculo
	 */
	public int getId() {
		return id;
	}

	/**
	 * Define o(a) id do(a) Veiculo
	 * @param id o(a) id a ser definido
	 */
	public void setId(int id) {
		this.id = id;
	}
    
    /**
     * Define a potencia em cavalos.
     * @param potenciaCavalos
     */
    public void setPotenciaCavalos(int potenciaCavalos)
    {
        this.potenciaCavalos = potenciaCavalos;
    }
    
    /**
     * Obt�m a potencia em cavalos.
     * @return
     */
    public int getPotenciaCavalos()
    {
        return this.potenciaCavalos;
    }
    
    /**
     * @see ItemVendido.setVendido()
     */
    public void setVendido(boolean vendido)
    {
        this.vendido = vendido;
    }
    
    /**
     * @see ItemVendido.getVendido()
     */
    public boolean getVendido()
    {
        return this.vendido;
    }
    
    /**
     * Define o valor o veiculo.
     * @param valor
     */
    public void setValor(float valor)
    {
        this.valor = valor;
    }
    
    /**
     * Obt�m o valor do veiculo.
     * @return
     */
    public float getValor()
    {
        return this.valor;
    }
    
    /**
     * Define o n�mero de marchas para o veiculo.
     * @param numeroMarcha
     */
    public void setNumeroMarcha(int numeroMarcha)
    {
        this.numeroMarcha = numeroMarcha;
    }
    
    /**
     * Obt�m o n�mero de marchas para o veiculo.
     * @return
     */
    public int getNumeroMarcha()
    {
        return this.numeroMarcha;
    }
    
    /**
     * Define a quantidade de portas que o veiculo tem.
     * @param qtdePortas
     */
    public void setQtdePortas(int qtdePortas)
    {
        this.qtdePortas = qtdePortas;
    }
    
    /**
     * Obt�m a quantidade de portas que o veiculo tem.
     * @return
     */
    public int getQtdePortas()
    {
        return this.qtdePortas;
    }
    
    /**
     * Define a quilometragem para o veiculo.
     * @param quiloMetros
     */
    public void setQuiloMetros(int quiloMetros)
    {
        this.quiloMetros = quiloMetros;
    }
    
    /**
     * Obt�m a quilometragem do veiculo.
     * @return
     */
    public int getQuiloMetros()
    {
        return this.quiloMetros;
    }
    
    /**
     * Define a cor para o veiculo.
     * @param cor
     */
    public void setCor(Cor cor)
    {
        this.cor = cor;
    }
    
    /**
     * Retorna a cor do veiculo.
     * @return
     */
    public Cor getCor()
    {
        return this.cor;
    }
    
    /**
     * Define o modelo para o veiculo.
     * @param modelo
     */
    public void setModelo(String modelo)
    {
        this.modelo = modelo;
    }
    
    /**
     * Obt�m o modelo do veiculo.
     * @return
     */
    public String getModelo()
    {
        return this.modelo;
    }
    
    /**
     * Define a marca para o veiculo.
     * @param marca
     */
    public void setMarca(Marca marca)
    {
        this.marca = marca;
    }
    
    /**
     * Obt�m a marca para o veiculo.
     * @return
     */
    public Marca getMarca()
    {
        return this.marca;
    }
    
    /**
     * Define o n�mero do chassi para o carro.
     * @param numeroChassi
     */
    public void setNumeroChassi(String numeroChassi)
    {
        this.numeroChassi = numeroChassi;
    }
    
    /**
     * Obt�m o n�mero de chassi.
     * @return
     */
    public String getNumeroChassi()
    {
        return this.numeroChassi;
    }
    
    /**
     * Define a placa do veiculo.
     * @param placa
     */
    public void setPlaca(String placa)
    {
        this.placa = placa;
    }
    
    /**
     * Obt�m a placa do veiculo.
     * @return
     */
    public String getPlaca()
    {
        return this.placa;
    }
    
    /**
     * Define o ano de fabrica��o para o veiculo.
     * @param anoFabricao
     */
    public void setAnoFabricacao(int anoFabricao)
    {
        this.anoFabricacao = anoFabricao;
    }
    
    /**
     * Obt�m o ano de fabrica��o do veiculo.
     * @return
     */
    public int getAnoFabricacao()
    {
        return this.anoFabricacao;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "anoFabricacao=" + anoFabricacao + ", placa=" + placa
				+ ", numeroChassi=" + numeroChassi + ", marca=" + marca
				+ ", modelo=" + modelo + ", cor=" + cor + ", quiloMetros="
				+ quiloMetros + ", qtdePortas=" + qtdePortas
				+ ", numeroMarcha=" + numeroMarcha + ", valor=" + valor
				+ ", vendido=" + vendido + ", potenciaCavalos="
				+ potenciaCavalos + "]";
	}
    
    
    
}
