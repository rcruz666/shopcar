package util;

import java.util.List;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Cor;
import entities.Marca;
import entities.Veiculo;

public class VeiculosTableModel extends AbstractTableModel {

	// Constantes que definem a ordem das colunas
	private final int COL_ID = 0;
	private final int COL_MODELO = 1;
	private final int COL_MARCA = 2;
	private final int COL_COR = 3;
	private final int COL_ANO_FABRICACAO = 4;
	private final int COL_PLACA = 5;
	private final int COL_CHASSI = 6;
	private final int COL_MARCHA = 7;
	private final int COL_CAVALOS = 8;
	private final int COL_PORTAS = 9;
	private final int COL_KM = 10;
	
	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
		
		/*this.addTableModelListener(new TableModelListener() {

			public void tableChanged(TableModelEvent e) {
				int linha = e.getFirstRow();
				Veiculo veiculo = VeiculosTableModel.this.veiculos.get(linha);
				
				DAOVeiculos.list.get(linha).setId(veiculo.getId());
				DAOVeiculos.list.get(linha).setAnoFabricacao(veiculo.getAnoFabricacao());
				DAOVeiculos.list.get(linha).setCor(veiculo.getCor());
				DAOVeiculos.list.get(linha).setMarca(veiculo.getMarca());
				DAOVeiculos.list.get(linha).setModelo(veiculo.getModelo());
				DAOVeiculos.list.get(linha).setNumeroChassi(veiculo.getNumeroChassi());
				DAOVeiculos.list.get(linha).setNumeroMarcha(veiculo.getNumeroMarcha());
				DAOVeiculos.list.get(linha).setPlaca(veiculo.getPlaca());
				DAOVeiculos.list.get(linha).setPotenciaCavalos(veiculo.getPotenciaCavalos());
				DAOVeiculos.list.get(linha).setQtdePortas(veiculo.getQtdePortas());
				DAOVeiculos.list.get(linha).setQuiloMetros(veiculo.getQuiloMetros());
				DAOVeiculos.list.get(linha).setValor(veiculo.getValor());
				DAOVeiculos.list.get(linha).setVendido(veiculo.getVendido());
			}
		});*/
		
		this.fireTableDataChanged();
	}

	private final int COL_VALOR = 11;
	private final int COL_VENDIDO = 12;

	private String[] colunas = { "Id", "Modelo", "Marca", "Cor",
			"Ano de fabrica��o", "Placa", "Chassi", "Qtd de marchas",
			"Pot�ncia em cavalos", "Qtd de portas", "Quilometragem", "Valor",
			"Vendido?" };
	
	private List<Veiculo> veiculos;

	private static final long serialVersionUID = 1L;

	public VeiculosTableModel(List<Veiculo> veiculos) {
		this.setVeiculos(veiculos);
	}

	/**
	 * Obt�m o(a) veiculos do(a) VeiculosTableModel
	 * @return o(a) veiculos do(a) VeiculosTableModel
	 */
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	// M�todos de acesso da lista de links
		
	public void inserir(Veiculo veiculo)
	{
		this.veiculos.add(veiculo);
		this.fireTableDataChanged();
	}
	
	public void excluir(int linha) 
	{
		this.veiculos.remove(linha);
		this.fireTableDataChanged();
	}
	
	public Veiculo get(int linha)
	{
		if (linha < 0 || linha >= veiculos.size()) {
			return null;
		}
		return this.veiculos.get(linha);
	}

	@Override
	public int getColumnCount() {
		return this.colunas.length;
	}

	@Override
	public String getColumnName(int index) 
	{
		return this.colunas[index];
	}

	@Override
	public int getRowCount() {
		return veiculos.size();
	}

	@Override
	public void setValueAt(Object valor, int linha, int coluna)
	{
		if (valor == null) return;
		
		switch (coluna) {
			case COL_ID: veiculos.get(linha).setAnoFabricacao(Integer.parseInt(valor.toString())); ; break;
			case COL_COR: veiculos.get(linha).setCor(Cor.valueOf(valor.toString())); ; break;
			case COL_MARCHA: veiculos.get(linha).setNumeroMarcha(Integer.parseInt(valor.toString())); ; break;
			case COL_MARCA: veiculos.get(linha).setMarca(Marca.valueOf(valor.toString())); ; break;
			case COL_MODELO: veiculos.get(linha).setModelo(valor.toString()); ; break;
			case COL_ANO_FABRICACAO: get(linha).setAnoFabricacao(Integer.parseInt(valor.toString()));
			case COL_CHASSI: veiculos.get(linha).setNumeroChassi(valor.toString()); ; break;
			case COL_PLACA: veiculos.get(linha).setPlaca(valor.toString()); ; break;
			case COL_CAVALOS: veiculos.get(linha).setPotenciaCavalos(Integer.parseInt(valor.toString())); ; break;
			case COL_PORTAS: veiculos.get(linha).setQtdePortas(Integer.parseInt(valor.toString())); ; break;
			case COL_KM: veiculos.get(linha).setQuiloMetros(Integer.parseInt(valor.toString())); ; break;
			case COL_VALOR: veiculos.get(linha).setValor(Float.parseFloat(valor.toString())); ; break;
			case COL_VENDIDO: veiculos.get(linha).setVendido((boolean)valor) ; break;
		}
		
		//this.fireTableRowsUpdated(linha, linha);
		this.fireTableDataChanged();
	}
		
	@Override
	public boolean isCellEditable(int linha, int coluna) 
	{
		/*if (coluna == COL_ID){
			return false;
		} else {
			return true;				
		}*/
		
		return false;
	}

	@Override
	public Object getValueAt(int linha, int coluna) 
	{
		try {
			System.out.println("Get value at, linha:" + linha);
			System.out.println(veiculos.toString());
			Veiculo veiculo = veiculos.get(linha);
			
			switch (coluna) {
				case COL_ID: return veiculo.getId();
				case COL_COR: return veiculo.getCor();
				case COL_MARCHA: return veiculo.getNumeroMarcha();
				case COL_MARCA: return veiculo.getMarca();
				case COL_MODELO: return veiculo.getModelo();
				case COL_CHASSI: return veiculo.getNumeroChassi();
				case COL_PLACA: return veiculo.getPlaca();
				case COL_CAVALOS: return veiculo.getPotenciaCavalos();
				case COL_ANO_FABRICACAO: return veiculo.getAnoFabricacao();
				case COL_PORTAS: return veiculo.getQtdePortas();
				case COL_KM: return veiculo.getQuiloMetros();
				case COL_VALOR: return veiculo.getValor();
				case COL_VENDIDO: return veiculo.getVendido();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
