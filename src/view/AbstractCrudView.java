package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Cor;
import entities.Marca;
import entities.Veiculo;

public abstract class AbstractCrudView<T extends Veiculo> extends AbstractView {
	
	protected T veiculo;
	protected DAO<T> dao;
	
	protected JPanel panel;
	
	protected JTextField txtQtdDePortas;
	protected JTextField txtAnoFabricacao;
	protected JTextField txtNumeroDaPlaca;
	protected JTextField txtNumeroDoChassi;
	protected JComboBox<Marca> cmbMarca;
	protected JTextField txtModelo;
	protected JLabel lblModelo;
	protected JComboBox<Cor> cmbCor;
	protected JLabel lblCor;
	protected JTextField txtQuilometragem;
	protected JLabel lblQuilometragem;
	protected JLabel lblPotnciaCv;
	protected JTextField txtPotenciaCavalos;
	protected JTextField txtNumeroMarchas;
	protected JLabel lblNmeroDeMarchas;
	protected JLabel lblQuantidadeDePortas;
	
	protected JLabel lblValorDoVeculo;
	protected JTextField txtValorVeiculo;
	
	protected JButton btnSalvar;
	protected JButton btnCancelar;
	
	public AbstractCrudView() {
		this.initialize();
	}
	
	/**
	 * Define o(a) carro do(a) CadastroDeCarroView
	 * @param carro o(a) carro a ser definido
	 */
	public void setCarro(T veiculo) {
		this.veiculo = veiculo;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblAnoDeFabricao = new JLabel("Ano de fabrica\u00E7\u00E3o:");
		lblAnoDeFabricao.setBounds(10, 11, 105, 24);
		panel.add(lblAnoDeFabricao);
		
		txtAnoFabricacao = new JTextField();
		txtAnoFabricacao.setBounds(115, 13, 76, 20);
		panel.add(txtAnoFabricacao);
		txtAnoFabricacao.setColumns(10);
		
		JLabel lblNmeroDaPlaca = new JLabel("N\u00FAmero da placa:");
		lblNmeroDaPlaca.setBounds(201, 11, 85, 24);
		panel.add(lblNmeroDaPlaca);
		
		txtNumeroDaPlaca = new JTextField();
		txtNumeroDaPlaca.setColumns(10);
		txtNumeroDaPlaca.setBounds(296, 13, 86, 20);
		panel.add(txtNumeroDaPlaca);
		
		txtNumeroDoChassi = new JTextField();
		txtNumeroDoChassi.setColumns(10);
		txtNumeroDoChassi.setBounds(115, 43, 267, 20);
		panel.add(txtNumeroDoChassi);
		
		JLabel lblNmeroDoChassi = new JLabel("N\u00FAmero do chassi:");
		lblNmeroDoChassi.setBounds(10, 41, 105, 24);
		panel.add(lblNmeroDoChassi);
		
		cmbMarca = new JComboBox<Marca>();
		for (Marca marca : Marca.values()) {
			cmbMarca.addItem(marca);
		}
		cmbMarca.setBounds(115, 73, 267, 20);
		panel.add(cmbMarca);
		
		JLabel lblMarca = new JLabel("Marca:");
		lblMarca.setBounds(10, 71, 105, 24);
		panel.add(lblMarca);
		
		txtModelo = new JTextField();
		txtModelo.setColumns(10);
		txtModelo.setBounds(115, 101, 267, 20);
		panel.add(txtModelo);
		
		lblModelo = new JLabel("Modelo:");
		lblModelo.setBounds(10, 99, 105, 24);
		panel.add(lblModelo);
		
		cmbCor = new JComboBox<Cor>();
		for (Cor cor : Cor.values()) {
			cmbCor.addItem(cor);
		}
		cmbCor.setBounds(115, 129, 267, 20);
		panel.add(cmbCor);
		
		lblCor = new JLabel("Cor:");
		lblCor.setBounds(10, 127, 105, 24);
		panel.add(lblCor);
		
		txtQuilometragem = new JTextField();
		txtQuilometragem.setColumns(10);
		txtQuilometragem.setBounds(115, 157, 267, 20);
		panel.add(txtQuilometragem);
		
		lblQuilometragem = new JLabel("Quilometragem:");
		lblQuilometragem.setBounds(10, 155, 105, 24);
		panel.add(lblQuilometragem);
		
		lblPotnciaCv = new JLabel("Pot\u00EAncia Cavalos:");
		lblPotnciaCv.setBounds(10, 183, 105, 24);
		panel.add(lblPotnciaCv);
		
		txtPotenciaCavalos = new JTextField();
		txtPotenciaCavalos.setColumns(10);
		txtPotenciaCavalos.setBounds(115, 185, 76, 20);
		panel.add(txtPotenciaCavalos);
		
		txtNumeroMarchas = new JTextField();
		txtNumeroMarchas.setColumns(10);
		txtNumeroMarchas.setBounds(306, 185, 76, 20);
		panel.add(txtNumeroMarchas);
		
		lblNmeroDeMarchas = new JLabel("N\u00FAmero de marchas:");
		lblNmeroDeMarchas.setBounds(201, 183, 105, 24);
		panel.add(lblNmeroDeMarchas);	
		
		lblQuantidadeDePortas = new JLabel("Qtd de portas:");
		lblQuantidadeDePortas.setBounds(201, 211, 105, 24);
		panel.add(lblQuantidadeDePortas);

		txtQtdDePortas = new JTextField();
		txtQtdDePortas.setColumns(10);
		txtQtdDePortas.setBounds(306, 213, 76, 20);
		panel.add(txtQtdDePortas);
		
		lblValorDoVeculo = new JLabel("Valor do ve\u00EDculo:");
		lblValorDoVeculo.setBounds(10, 211, 105, 24);
		panel.add(lblValorDoVeculo);
		
		txtValorVeiculo = new JTextField();
		txtValorVeiculo.setColumns(10);
		txtValorVeiculo.setBounds(115, 213, 76, 20);
		panel.add(txtValorVeiculo);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(102, 281, 89, 23);
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AbstractCrudView.this.cadastrarVeiculo();
			}
		});
		panel.add(btnSalvar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(197, 281, 89, 23);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				frame.setVisible(false);
			}
		});
		panel.add(btnCancelar);
		
	}

	protected void cadastrarVeiculo() {

		try {
			veiculo.setAnoFabricacao(Integer.parseInt(txtAnoFabricacao.getText()));
			veiculo.setCor((Cor)cmbCor.getSelectedItem());
			veiculo.setNumeroMarcha(Integer.parseInt(txtNumeroMarchas.getText()));
			veiculo.setMarca((Marca)cmbMarca.getSelectedItem());
			veiculo.setModelo(txtModelo.getText());
			veiculo.setNumeroChassi(txtNumeroDoChassi.getText());
			veiculo.setPlaca(txtNumeroDaPlaca.getText());
			veiculo.setPotenciaCavalos(Integer.parseInt(txtPotenciaCavalos.getText()));
			veiculo.setQtdePortas(Integer.parseInt(txtQtdDePortas.getText()));
			veiculo.setQuiloMetros(Integer.parseInt(txtQuilometragem.getText()));
			veiculo.setValor(Float.parseFloat(txtValorVeiculo.getText()));
			
			// cadastrarDadosEspecificos();
			
			dao.salvar(veiculo);
			
			JOptionPane.showMessageDialog(frame, 
					"Ve�culo salvo com sucesso.", "Aviso",
					JOptionPane.NO_OPTION
			);
			frame.setVisible(false);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			JOptionPane.showMessageDialog(frame, 
					"Ve�culo salvo com sucesso.", "Aviso",
				JOptionPane.NO_OPTION
			);
		}
	}
	
	protected abstract void cadastrarDadosEspecificos();
}
