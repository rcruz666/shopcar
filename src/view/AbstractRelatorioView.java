package view;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import util.VeiculosTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public abstract class AbstractRelatorioView extends AbstractView {
	
	protected JTable tblVeiculos;
	protected VeiculosTableModel tableModel;

	/**
	 * Create the application.
	 */
	public AbstractRelatorioView(VeiculosTableModel tableModel) {
		this.tableModel = tableModel;
		this.initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 50, 780, 262);
		frame.getContentPane().add(scrollPane);
		
		tblVeiculos = new JTable(tableModel);
		scrollPane.setViewportView(tblVeiculos);
		
		JButton btnCancelar = new JButton("Fechar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				frame.setVisible(false);
			}
		});
		btnCancelar.setBounds(400, 400, 89, 23);
		frame.getContentPane().add(btnCancelar);
		frame.setBounds(100, 100, 850, 500);
	}
}
