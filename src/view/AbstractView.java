package view;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public abstract class AbstractView {
	
	protected JFrame frame;	
	
	public void exibirFrame() {
		
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	protected void atualizarFrame() {
		
		SwingUtilities.updateComponentTreeUI(frame);
		
		frame.invalidate();
		frame.validate();
		frame.repaint();
	}
	
}
