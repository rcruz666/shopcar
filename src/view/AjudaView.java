package view;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AjudaView extends AbstractView {

	public AjudaView() {
		initialize();
	}


	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Ajuda");
		frame.setBounds(100, 100, 450, 300);
		frame.getContentPane().setLayout(null);
		
		JTextPane txtpnCadastrosNo = new JTextPane();
		txtpnCadastrosNo.setBackground(SystemColor.control);
		txtpnCadastrosNo.setText("- Cadastros\r\n\r\n  No  menu Cadastro \u00E9 poss\u00EDvel cadastrar novos ve\u00EDculos. Para isso, preencha os dados do ve\u00EDculo e clique em \"Salvar\". Clique em \"Sair\" para voltar ao menu principal.\r\n\r\n- Relat\u00F3rios\r\n\r\n  No menu Relat\u00F3rios \u00E9 poss\u00EDvel escolher entre diversos tipos de relat\u00F3rios sobre os ve\u00EDculos cadastrados no sistema. Escolha um tipo de relat\u00F3rio e, se necess\u00E1rio, insira a informa\u00E7\u00E3o de filtragem e clique em \"Pesquisar\" para visualizar os ve\u00EDculos correspondentes. Clique em \"Sair\" para voltar ao menu principal.");
		txtpnCadastrosNo.setBounds(10, 11, 414, 210);
		frame.getContentPane().add(txtpnCadastrosNo);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				frame.setVisible(false);
			}
		});
		btnSair.setBounds(162, 227, 89, 23);
		frame.getContentPane().add(btnSair);
	}
}
