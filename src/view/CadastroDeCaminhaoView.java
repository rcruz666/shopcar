package view;

import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DAO;
import entities.Caminhao;

public class CadastroDeCaminhaoView extends AbstractCrudView<Caminhao> {
		
	private JLabel lblCapacidadeMaxCarga;
	private JTextField txtCapacidadeMaxCarga;
	
	private JLabel lblNmeroDeEixos;
	private JTextField txtNumeroDeEixos;

	private JLabel lblTipoDeCarroceria;
	private JTextField txtTipoDeCarroceria;

	/**
	 * Create the application.
	 */
	public CadastroDeCaminhaoView() {
		super();
		
		veiculo = new Caminhao();

		try
		{
			dao = new DAO<Caminhao>();
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		
		this.initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame.setTitle("Cadastro de Caminh\u00E3o");
		frame.setBounds(100, 100, 415, 393);
		
		lblCapacidadeMaxCarga = new JLabel("Capacidade M\u00E1xima de Carga:");
		lblCapacidadeMaxCarga.setBounds(38, 246, 153, 24);
		panel.add(lblCapacidadeMaxCarga);
		
		txtCapacidadeMaxCarga = new JTextField();
		txtCapacidadeMaxCarga.setColumns(10);
		txtCapacidadeMaxCarga.setBounds(201, 248, 181, 20);
		panel.add(txtCapacidadeMaxCarga);
		
		lblNmeroDeEixos = new JLabel("N\u00FAmero de eixos:");
		lblNmeroDeEixos.setBounds(23, 273, 86, 24);
		panel.add(lblNmeroDeEixos);
		
		txtNumeroDeEixos = new JTextField();
		txtNumeroDeEixos.setColumns(10);
		txtNumeroDeEixos.setBounds(115, 275, 76, 20);
		panel.add(txtNumeroDeEixos);
				
		lblTipoDeCarroceria = new JLabel("Tipo de carroceria:");
		lblTipoDeCarroceria.setBounds(201, 273, 98, 24);
		panel.add(lblTipoDeCarroceria);

		txtTipoDeCarroceria = new JTextField();
		txtTipoDeCarroceria.setColumns(10);
		txtTipoDeCarroceria.setBounds(306, 275, 76, 20);
		panel.add(txtTipoDeCarroceria);
		
		btnSalvar.setBounds(102, 320, 89, 23);
		btnCancelar.setBounds(197, 320, 89, 23);
	}

	@Override
	protected void cadastrarDadosEspecificos() {
		veiculo.setCapacidadeMaxCarga( Integer.parseInt(txtCapacidadeMaxCarga.getText()));
		veiculo.setNumeroEixos(Integer.parseInt(txtNumeroDeEixos.getText()));
		veiculo.setTipoCarroceria(txtTipoDeCarroceria.getText());
	}
}
