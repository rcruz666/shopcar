package view;

import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DAO;
import entities.Caminhonete;

public class CadastroDeCaminhoneteView extends AbstractCrudView<Caminhonete> {

	private JTextField txtCapacidadeMaxCarga;
	private JTextField txtNumeroDeEixos;

	/**
	 * Create the application.
	 */
	public CadastroDeCaminhoneteView() {
		super();
		veiculo = new Caminhonete();
		try
		{
			dao = new DAO<Caminhonete>();
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		this.initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame.setTitle("Cadastro de Caminh\u00E3onete");
		frame.setBounds(100, 100, 415, 393);

		JLabel lblNmeroDeEixos = new JLabel("Capacidade M\u00E1xima de Carga:");
		lblNmeroDeEixos.setBounds(38, 246, 153, 24);
		panel.add(lblNmeroDeEixos);
		
		txtCapacidadeMaxCarga = new JTextField();
		txtCapacidadeMaxCarga.setColumns(10);
		txtCapacidadeMaxCarga.setBounds(201, 248, 181, 20);
		panel.add(txtCapacidadeMaxCarga);
		
		JLabel lblNumeroDeEixos = new JLabel("N\u00FAmero de eixos:");
		lblNumeroDeEixos.setBounds(23, 273, 86, 24);
		panel.add(lblNumeroDeEixos);

		txtNumeroDeEixos = new JTextField();
		txtNumeroDeEixos.setColumns(10);
		txtNumeroDeEixos.setBounds(115, 275, 76, 20);
		panel.add(txtNumeroDeEixos);

		btnSalvar.setBounds(102, 320, 89, 23);
		btnCancelar.setBounds(197, 320, 89, 23);

	}

	@Override
	protected void cadastrarDadosEspecificos() {
		veiculo.setCapacidadeMaxCarga(Integer.parseInt(txtCapacidadeMaxCarga.getText()));
		veiculo.setNumeroEixos(Integer.parseInt(txtNumeroDeEixos.getText()));
	}
}
