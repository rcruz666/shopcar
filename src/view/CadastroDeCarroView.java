package view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DAO;
import entities.Carro;

public class CadastroDeCarroView extends AbstractCrudView<Carro> {
	
	private JLabel lblNmeroDeEixos;
	private JTextField txtNumeroDeEixos;
	
	/**
	 * Create the application.
	 */
	public CadastroDeCarroView() {
		super();
		
		veiculo = new Carro();
		try
		{
			dao = new DAO<Carro>();
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}		
		
		frame.setTitle("Cadastro de Carro");
		frame.setBounds(100, 100, 415, 360);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		lblNmeroDeEixos = new JLabel("N\u00FAmero de eixos:");
		lblNmeroDeEixos.setBounds(10, 246, 105, 24);
		panel.add(lblNmeroDeEixos);

		txtNumeroDeEixos = new JTextField();
		txtNumeroDeEixos.setColumns(10);
		txtNumeroDeEixos.setBounds(115, 248, 76, 20);
		panel.add(txtNumeroDeEixos);
	}

	@Override
	protected void cadastrarDadosEspecificos() {
		// veiculo.setNumeroEixos(Integer.parseInt(txtNumeroDeEixos.getText()));
	}
}
