package view;

import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DAO;
import entities.Moto;
import entities.Onibus;

public class CadastroDeMotoView extends AbstractCrudView<Moto> {

	private JLabel lblCilindradas;
	private JTextField txtCilindradas;

	/**
	 * Create the application.
	 */
	public CadastroDeMotoView() {
		super();
		
		veiculo = new Moto();
		try
		{
			dao = new DAO<Moto>();
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame.setTitle("Cadastro de Moto");
		frame.setBounds(100, 100, 415, 360);
		
		lblCilindradas = new JLabel("Cilindradas:");
		lblCilindradas.setBounds(10, 246, 105, 24);
		panel.add(lblCilindradas);
		
		txtCilindradas = new JTextField();
		txtCilindradas.setColumns(10);
		txtCilindradas.setBounds(115, 248, 267, 20);
		panel.add(txtCilindradas);
		
		btnSalvar.setBounds(102, 281, 89, 23);

		btnCancelar.setBounds(197, 281, 89, 23);
	}

	@Override
	protected void cadastrarDadosEspecificos() {
		veiculo.setCilindradas(Integer.parseInt(txtCilindradas.getText()));
	}
}
