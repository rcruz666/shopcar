package view;

import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DAO;
import entities.Onibus;


public class CadastroDeOnibusView extends AbstractCrudView<Onibus> {

	private JTextField txtNumeroDeEixos;
	private JTextField txtNumeroDeAssentos;

	/**
	 * Create the application.
	 */
	public CadastroDeOnibusView() {
		super();
		
		veiculo = new Onibus();
		try
		{
			dao = new DAO<Onibus>();
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame.setTitle("Cadastro de \u00D4nibus");
		frame.setBounds(100, 100, 415, 393);

		txtNumeroDeEixos = new JTextField();
		txtNumeroDeEixos.setColumns(10);
		txtNumeroDeEixos.setBounds(115, 242, 76, 20);
		panel.add(txtNumeroDeEixos);
		
		JLabel lblNmeroDePortas = new JLabel("N\u00FAmero de eixos:");
		lblNmeroDePortas.setBounds(23, 240, 86, 24);
		panel.add(lblNmeroDePortas);

		txtNumeroDeAssentos = new JTextField();
		txtNumeroDeAssentos.setColumns(10);
		txtNumeroDeAssentos.setBounds(316, 242, 66, 20);
		panel.add(txtNumeroDeAssentos);
		
		JLabel lblNumeroDeAssentos = new JLabel("N\u00FAmero de assentos:");
		lblNumeroDeAssentos.setBounds(201, 240, 105, 24);
		panel.add(lblNumeroDeAssentos);
		
		btnSalvar.setBounds(102, 320, 89, 23);
		btnCancelar.setBounds(197, 320, 89, 23);
	}

	@Override
	protected void cadastrarDadosEspecificos() {
		veiculo.setNumeroAssentos(Integer.parseInt(txtNumeroDeAssentos.getText()));
		veiculo.setNumeroEixos(Integer.parseInt(txtNumeroDeEixos.getText()));
	}
}
