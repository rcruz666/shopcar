package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Veiculo;

import javax.swing.JSeparator;
import javax.swing.JCheckBox;

public class FichaVeiculoView extends AbstractView {
	
	protected Veiculo veiculo;
	protected DAO<Veiculo> dao;
	
	protected JPanel panel;
	
	protected JTextField txtQtdDePortas;
	protected JTextField txtAnoFabricacao;
	protected JTextField txtNumeroDaPlaca;
	protected JTextField txtNumeroDoChassi;
	protected JTextField txtMarca;
	protected JTextField txtModelo;
	protected JLabel lblModelo;
	protected JTextField txtCor;
	protected JLabel lblCor;
	protected JTextField txtQuilometragem;
	protected JLabel lblQuilometragem;
	protected JLabel lblPotnciaCv;
	protected JTextField txtPotenciaCavalos;
	protected JTextField txtNumeroMarchas;
	protected JLabel lblNmeroDeMarchas;
	protected JLabel lblQuantidadeDePortas;
	
	protected JLabel lblValorDoVeculo;
	protected JTextField txtValorVeiculo;
	protected JButton btnSair;
	private JTextField txtPlacaProcurada;
	private JButton btnPesquisar;
	private JLabel lblVendido;
	private JCheckBox ckbVendido;
	
	public FichaVeiculoView() {
		try
		{
			dao = new DAO<Veiculo>();
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		this.initialize();
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblAnoDeFabricao = new JLabel("Ano de fabrica\u00E7\u00E3o:");
		lblAnoDeFabricao.setBounds(30, 65, 105, 24);
		panel.add(lblAnoDeFabricao);
		
		txtAnoFabricacao = new JTextField();
		txtAnoFabricacao.setEnabled(false);
		txtAnoFabricacao.setBounds(135, 67, 76, 20);
		panel.add(txtAnoFabricacao);
		txtAnoFabricacao.setColumns(10);
		
		JLabel lblNmeroDaPlaca = new JLabel("N\u00FAmero da placa:");
		lblNmeroDaPlaca.setBounds(221, 65, 85, 24);
		panel.add(lblNmeroDaPlaca);
		
		txtNumeroDaPlaca = new JTextField();
		txtNumeroDaPlaca.setEnabled(false);
		txtNumeroDaPlaca.setColumns(10);
		txtNumeroDaPlaca.setBounds(316, 67, 86, 20);
		panel.add(txtNumeroDaPlaca);
		
		txtNumeroDoChassi = new JTextField();
		txtNumeroDoChassi.setEnabled(false);
		txtNumeroDoChassi.setColumns(10);
		txtNumeroDoChassi.setBounds(135, 97, 267, 20);
		panel.add(txtNumeroDoChassi);
		
		JLabel lblNmeroDoChassi = new JLabel("N\u00FAmero do chassi:");
		lblNmeroDoChassi.setBounds(30, 95, 105, 24);
		panel.add(lblNmeroDoChassi);
		
		txtMarca = new JTextField();
		txtMarca.setEnabled(false);
		txtMarca.setColumns(10);
		txtMarca.setBounds(135, 127, 267, 20);
		panel.add(txtMarca);
		
		JLabel lblMarca = new JLabel("Marca:");
		lblMarca.setBounds(30, 125, 105, 24);
		panel.add(lblMarca);
		
		txtModelo = new JTextField();
		txtModelo.setEnabled(false);
		txtModelo.setColumns(10);
		txtModelo.setBounds(135, 155, 267, 20);
		panel.add(txtModelo);
		
		lblModelo = new JLabel("Modelo:");
		lblModelo.setBounds(30, 153, 105, 24);
		panel.add(lblModelo);
		
		txtCor = new JTextField();
		txtCor.setEnabled(false);
		txtCor.setColumns(10);
		txtCor.setBounds(135, 183, 267, 20);
		panel.add(txtCor);
		
		lblCor = new JLabel("Cor:");
		lblCor.setBounds(30, 181, 105, 24);
		panel.add(lblCor);
		
		txtQuilometragem = new JTextField();
		txtQuilometragem.setEnabled(false);
		txtQuilometragem.setColumns(10);
		txtQuilometragem.setBounds(135, 211, 267, 20);
		panel.add(txtQuilometragem);
		
		lblQuilometragem = new JLabel("Quilometragem:");
		lblQuilometragem.setBounds(30, 209, 105, 24);
		panel.add(lblQuilometragem);
		
		lblPotnciaCv = new JLabel("Pot\u00EAncia Cavalos:");
		lblPotnciaCv.setBounds(30, 237, 105, 24);
		panel.add(lblPotnciaCv);
		
		txtPotenciaCavalos = new JTextField();
		txtPotenciaCavalos.setEnabled(false);
		txtPotenciaCavalos.setColumns(10);
		txtPotenciaCavalos.setBounds(135, 239, 76, 20);
		panel.add(txtPotenciaCavalos);
		
		txtNumeroMarchas = new JTextField();
		txtNumeroMarchas.setEnabled(false);
		txtNumeroMarchas.setColumns(10);
		txtNumeroMarchas.setBounds(326, 239, 76, 20);
		panel.add(txtNumeroMarchas);
		
		lblNmeroDeMarchas = new JLabel("N\u00FAmero de marchas:");
		lblNmeroDeMarchas.setBounds(221, 237, 105, 24);
		panel.add(lblNmeroDeMarchas);	
		
		lblQuantidadeDePortas = new JLabel("Qtd de portas:");
		lblQuantidadeDePortas.setBounds(221, 265, 105, 24);
		panel.add(lblQuantidadeDePortas);

		txtQtdDePortas = new JTextField();
		txtQtdDePortas.setEnabled(false);
		txtQtdDePortas.setColumns(10);
		txtQtdDePortas.setBounds(326, 267, 76, 20);
		panel.add(txtQtdDePortas);
		
		lblValorDoVeculo = new JLabel("Valor do ve\u00EDculo:");
		lblValorDoVeculo.setBounds(30, 265, 105, 24);
		panel.add(lblValorDoVeculo);
		
		txtValorVeiculo = new JTextField();
		txtValorVeiculo.setEnabled(false);
		txtValorVeiculo.setColumns(10);
		txtValorVeiculo.setBounds(135, 267, 76, 20);
		panel.add(txtValorVeiculo);
		
		btnSair = new JButton("Sair");
		btnSair.setBounds(189, 344, 89, 23);
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				frame.setVisible(false);
			}
		});
		panel.add(btnSair);
		
		JLabel lblInsiraOChassi = new JLabel("Insira a Placa do ve\u00EDculo:");
		lblInsiraOChassi.setBounds(30, 22, 135, 14);
		panel.add(lblInsiraOChassi);
		
		txtPlacaProcurada = new JTextField();
		txtPlacaProcurada.setBounds(160, 19, 140, 20);
		panel.add(txtPlacaProcurada);
		txtPlacaProcurada.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(30, 47, 372, 7);
		panel.add(separator);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				FichaVeiculoView.this.obterFichaVeiculo(txtPlacaProcurada.getText());
			}
		});
		btnPesquisar.setBounds(313, 18, 89, 23);
		panel.add(btnPesquisar);
		
		lblVendido = new JLabel("Vendido:");
		lblVendido.setBounds(30, 300, 46, 14);
		panel.add(lblVendido);
		
		ckbVendido = new JCheckBox("");
		ckbVendido.setEnabled(false);
		ckbVendido.setBounds(82, 296, 27, 23);
		panel.add(ckbVendido);
		
		frame.setBounds(0, 0, 500, 430);		
	}

	protected void obterFichaVeiculo(String placa) {
		try{
			this.veiculo = DAOVeiculos.getPlaca(placa);
			
			txtAnoFabricacao.setText(String.valueOf(veiculo.getAnoFabricacao()));
			txtNumeroDaPlaca.setText(veiculo.getPlaca());
			txtNumeroDoChassi.setText(veiculo.getNumeroChassi());
			txtMarca.setText(veiculo.getMarca().toString());
			txtModelo.setText(veiculo.getModelo());
			txtCor.setText(veiculo.getCor().toString());
			txtQuilometragem.setText(String.valueOf(veiculo.getQuiloMetros()));
			txtPotenciaCavalos.setText(String.valueOf(veiculo.getPotenciaCavalos()));
			txtNumeroMarchas.setText(String.valueOf(veiculo.getNumeroMarcha()));
			txtValorVeiculo.setText(String.valueOf(veiculo.getValor()));
			txtQtdDePortas.setText(String.valueOf(veiculo.getQtdePortas()));
			ckbVendido.setSelected(veiculo.getVendido());
			
			atualizarFrame();
		} catch(Exception e) {
			JOptionPane.showMessageDialog(frame, 
				"Forne�a os dados corretos para a pesquisa.", "Aviso",
				JOptionPane.NO_OPTION
			);
		}
	}
}
