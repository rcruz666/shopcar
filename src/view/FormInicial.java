package view;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import java.awt.event.ActionListener;

/**
 * Formulário inicial da aplicação. 
 */
public class FormInicial extends AbstractView {
	
	public FormInicial() {

		frame = new JFrame();
		frame.setTitle("Simulação de Software Proprietário");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		this.criarMenuPrincipal();		
	}
	
	// Menu principal
	private JMenuBar mnbMenuPrincipal;
	
	private void criarMenuPrincipal() {
		
		mnbMenuPrincipal = new JMenuBar();
		frame.setJMenuBar(mnbMenuPrincipal);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		mnbMenuPrincipal.add(mnCadastro);
		
		JMenuItem mntmCarros = new JMenuItem("Carros");
		mntmCarros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				CadastroDeCarroView cadastroDeCarroView = new CadastroDeCarroView();
				cadastroDeCarroView.exibirFrame();
				
			}
		});
		mnCadastro.add(mntmCarros);
		
		JMenuItem mntmCaminhao = new JMenuItem("Caminh\u00E3o");
		mntmCaminhao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				CadastroDeCaminhaoView cadastroDeCaminhaoView = new CadastroDeCaminhaoView();
				cadastroDeCaminhaoView.exibirFrame();
			}
		});
		mnCadastro.add(mntmCaminhao);
		
		JMenuItem mntmCaminhonete = new JMenuItem("Caminhonete");
		mntmCaminhonete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				CadastroDeCaminhoneteView cadastroDeCaminhoneteView = new CadastroDeCaminhoneteView();
				cadastroDeCaminhoneteView.exibirFrame();
			}
		});
		mnCadastro.add(mntmCaminhonete);
		
		JMenuItem mntmOnibus = new JMenuItem("\u00D4nibus");
		mntmOnibus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastroDeOnibusView cadastroDeOnibusView = new CadastroDeOnibusView();
				cadastroDeOnibusView.exibirFrame();
			}
		});
		mnCadastro.add(mntmOnibus);
		
		JMenuItem mntmMotos = new JMenuItem("Motos");
		mntmMotos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastroDeMotoView cadastroDeMotoView = new CadastroDeMotoView();
				cadastroDeMotoView.exibirFrame();
			}
		});
		mnCadastro.add(mntmMotos);
		
		JSeparator separator = new JSeparator();
		mnCadastro.add(separator);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mnCadastro.add(mntmSair);
		
		JMenu mnRelatrios = new JMenu("Relat\u00F3rios");
		mnbMenuPrincipal.add(mnRelatrios);
		
		JMenuItem mntmVeculosNoVendidos = new JMenuItem("Ve\u00EDculos N\u00E3o Vendidos");
		mntmVeculosNoVendidos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new RelatorioVeiculosNaoVendidosView().exibirFrame();
			}
		});
		mnRelatrios.add(mntmVeculosNoVendidos);
		
		JMenuItem mntmVeculosAno = new JMenuItem("Ve\u00EDculos / Ano");
		mntmVeculosAno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new RelatorioVeiculosPorAnoView().exibirFrame();
			}
		});
		mnRelatrios.add(mntmVeculosAno);
		
		JMenuItem mntmVeculosModelo = new JMenuItem("Ve\u00EDculos / Modelo");
		mntmVeculosModelo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new RelatorioVeiculosPorModeloView().exibirFrame();
			}
		});
		mnRelatrios.add(mntmVeculosModelo);
		
		JMenuItem mntmVeiculosMarca = new JMenuItem("Ve\u00EDculos / Marca");
		mntmVeiculosMarca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new RelatorioVeiculosPorMarcaView().exibirFrame();
			}
		});
		mnRelatrios.add(mntmVeiculosMarca);
		
		JMenuItem mntmVeiculosQuilometragem = new JMenuItem("Ve\u00EDculos / Quilometragem");
		mntmVeiculosQuilometragem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				new RelatorioVeiculosPorKmView().exibirFrame();
			}
		});
		mnRelatrios.add(mntmVeiculosQuilometragem);
		
		JMenuItem mntmFichaDoVeculo = new JMenuItem("Ficha do Ve\u00EDculo");
		mntmFichaDoVeculo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new FichaVeiculoView().exibirFrame();
			}
		});
		mnRelatrios.add(mntmFichaDoVeculo);
		
		JMenu mnAjuda = new JMenu("Ajuda");
		mnbMenuPrincipal.add(mnAjuda);
		
		JMenuItem mntmAjuda = new JMenuItem("Ajuda");
		mntmAjuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AjudaView().exibirFrame();
			}
		});
		mnAjuda.add(mntmAjuda);
		
		JMenuItem mntmSobre = new JMenuItem("Sobre");
		mntmSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new SobreView().exibirFrame();
			}
		});
		mnAjuda.add(mntmSobre);
		
		ImageIcon imagem = new ImageIcon(this.getClass().getResource("/assets/shopcar.jpg")); 
		JLabel lblImg = new JLabel(imagem);
		lblImg.setSize(imagem.getIconWidth(), imagem.getIconHeight());
		frame.getContentPane().add(lblImg);
		frame.setSize(imagem.getIconWidth(), imagem.getIconHeight()+50);

	}
}
