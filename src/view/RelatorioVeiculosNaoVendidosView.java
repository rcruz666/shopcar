package view;

import dao.*;
import util.VeiculosTableModel;

public class RelatorioVeiculosNaoVendidosView extends AbstractRelatorioView {

	public RelatorioVeiculosNaoVendidosView() {
		super(new VeiculosTableModel(DAOVeiculos.getNaoVendidos()));
		try
		{
			frame.setTitle("Ve\u00EDculos n\u00E3o vendidos");
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		
	}

}
