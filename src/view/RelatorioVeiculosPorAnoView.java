package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Veiculo;
import util.VeiculosTableModel;

public class RelatorioVeiculosPorAnoView extends AbstractRelatorioView {
	
	private JTextField txtAno;
	private JButton btnPesquisar;
	
	public RelatorioVeiculosPorAnoView() {
		super(new VeiculosTableModel(DAOVeiculos.list));
		frame.setTitle("Ve\u00EDculos por ano");
		
		JLabel lblAno = new JLabel("Insira o ano:");
		lblAno.setBounds(10,10,200,20);
		frame.getContentPane().add(lblAno);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.setBounds(220, 30, 150, 20);
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				RelatorioVeiculosPorAnoView.this.pesquisarVeiculosPorAno();
			}			
		});
		frame.getContentPane().add(btnPesquisar);
		
		txtAno = new JTextField();
		txtAno.setColumns(10);
		txtAno.setBounds(10, 30, 200, 20);
		frame.getContentPane().add(txtAno);
		
	}
	
	private void pesquisarVeiculosPorAno() {
		int ano = 0;
		try{
			try {
				ano = Integer.parseInt(txtAno.getText());
				System.out.println(ano);
			} catch(Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(frame, 
					"Forne�a os dados corretos para a pesquisa.", "Aviso",
					JOptionPane.NO_OPTION
				);
			}
			
			List<Veiculo> veiculos = DAOVeiculos.getPorAno(ano);
			if (veiculos.size() != 0) {
				VeiculosTableModel tblModel = (VeiculosTableModel) tblVeiculos.getModel();
				tblModel.setVeiculos(veiculos);				
			} else {
				JOptionPane.showMessageDialog(frame, 
						"Nenhum ve�culo encontrado.", "Aviso",
						JOptionPane.NO_OPTION
					);
			}						
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			txtAno.setText("");
			atualizarFrame();
		}
	}

}
