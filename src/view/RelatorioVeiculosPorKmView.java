package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Veiculo;
import util.VeiculosTableModel;

public class RelatorioVeiculosPorKmView extends AbstractRelatorioView {
	
	private JTextField txtKm;
	private JButton btnPesquisar;
	
	public RelatorioVeiculosPorKmView() {
		super(new VeiculosTableModel(DAOVeiculos.list));
		frame.setTitle("Ve\u00EDculos por quilometragem");
		
		JLabel label = new JLabel("Insira a quilometragem:");
		label.setBounds(10,10,200,20);
		frame.getContentPane().add(label);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.setBounds(220, 30, 150, 20);
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				RelatorioVeiculosPorKmView.this.pesquisarVeiculosPorKm();
			}			
		});
		frame.getContentPane().add(btnPesquisar);
		
		txtKm = new JTextField();
		txtKm.setColumns(10);
		txtKm.setBounds(10, 30, 200, 20);
		frame.getContentPane().add(txtKm);
		
	}
	
	private void pesquisarVeiculosPorKm() {
		try{			
			int km = Integer.parseInt(txtKm.getText());
			List<Veiculo> veiculos = DAOVeiculos.getPorKM(km);
			if (veiculos.size() > 0) {
				VeiculosTableModel tblModel = (VeiculosTableModel) tblVeiculos.getModel();
				tblModel.setVeiculos(veiculos);
			}
			atualizarFrame();
		} catch(Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(frame, 
				"Forne�a os dados corretos para a pesquisa.", "Aviso",
				JOptionPane.NO_OPTION
			);
		}
	}

}
