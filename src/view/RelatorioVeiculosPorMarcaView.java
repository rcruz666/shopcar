package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Marca;
import entities.Veiculo;
import util.VeiculosTableModel;

public class RelatorioVeiculosPorMarcaView extends AbstractRelatorioView {
	
	private JComboBox<Marca> cmbMarca;
	private JButton btnPesquisar;
	
	public RelatorioVeiculosPorMarcaView() {
		super(new VeiculosTableModel(DAOVeiculos.list));
		frame.setTitle("Ve\u00EDculos por marca");
		
		JLabel label = new JLabel("Insira a marca:");
		label.setBounds(10,10,200,20);
		frame.getContentPane().add(label);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.setBounds(220, 30, 150, 20);
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				RelatorioVeiculosPorMarcaView.this.pesquisarVeiculosPorMarca();
			}			
		});
		frame.getContentPane().add(btnPesquisar);
		
		cmbMarca = new JComboBox<Marca>();
		for (Marca marca : Marca.values()) {
			cmbMarca.addItem(marca);
		}
		cmbMarca.setBounds(10, 30, 200, 20);
		frame.getContentPane().add(cmbMarca);
		
	}
	
	private void pesquisarVeiculosPorMarca() {
		try {
			Marca marca = (Marca) cmbMarca.getSelectedItem();
			List<Veiculo> veiculos = DAOVeiculos.getPorMarca(marca);
			if (veiculos.size() > 0) {
				VeiculosTableModel tblModel = (VeiculosTableModel) tblVeiculos.getModel();
				tblModel.setVeiculos(veiculos);
			}
			atualizarFrame();			
		} catch(Exception e) {
			JOptionPane.showMessageDialog(frame, 
				"Forne�a os dados corretos para a pesquisa.", "Aviso",
				JOptionPane.NO_OPTION
			);
		}
	}

}
