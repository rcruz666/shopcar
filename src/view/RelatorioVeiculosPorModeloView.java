package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.DAO;
import dao.DAOVeiculos;
import entities.Veiculo;
import util.VeiculosTableModel;

public class RelatorioVeiculosPorModeloView extends AbstractRelatorioView {
	
	private JTextField txtModelo;
	private JButton btnPesquisar;
	
	public RelatorioVeiculosPorModeloView() {
		super(new VeiculosTableModel(DAOVeiculos.list));
		frame.setTitle("Ve\u00EDculos por modelo");
		
		JLabel label = new JLabel("Insira o modelo:");
		label.setBounds(10,10,200,20);
		frame.getContentPane().add(label);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.setBounds(220, 30, 150, 20);
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				RelatorioVeiculosPorModeloView.this.pesquisarVeiculosPorModelo();
			}			
		});
		frame.getContentPane().add(btnPesquisar);
		
		txtModelo = new JTextField();
		txtModelo.setColumns(10);
		txtModelo.setBounds(10, 30, 200, 20);
		frame.getContentPane().add(txtModelo);
		
	}
	
	private void pesquisarVeiculosPorModelo() {
		try {
			String modelo = txtModelo.getText();		
			List<Veiculo> veiculos = DAOVeiculos.getPorModelo(modelo);
			if (veiculos.size() > 0) {
				VeiculosTableModel tblModel = (VeiculosTableModel) tblVeiculos.getModel();
				tblModel.setVeiculos(veiculos);
			}
			atualizarFrame();			
		} catch(Exception e) {
			JOptionPane.showMessageDialog(frame, 
				"Forne�a os dados corretos para a pesquisa.", "Aviso",
				JOptionPane.NO_OPTION
			);
		}
	}

}
