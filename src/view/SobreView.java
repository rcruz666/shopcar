package view;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SobreView extends AbstractView {

	public SobreView() {
		initialize();
	}
	

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Sobre");
		frame.setBounds(100, 100, 450, 300);
		frame.getContentPane().setLayout(null);
		
		JTextPane txtpnCadastrosNo = new JTextPane();
		txtpnCadastrosNo.setBackground(SystemColor.control);
		txtpnCadastrosNo.setText("Sobre\n\n\rNome do Sistema: ShopCar\n\n\rVers�o:0.1\n\n\rDesenvolvido por Carlos Henrique(colocar prontu�rio), Rafael Cruz (126209-2) e Argemiro (colocar prontu�rio).");
		txtpnCadastrosNo.setBounds(10, 11, 414, 210);
		frame.getContentPane().add(txtpnCadastrosNo);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				frame.setVisible(false);
			}
		});
		btnSair.setBounds(162, 227, 89, 23);
		frame.getContentPane().add(btnSair);
	}
}
